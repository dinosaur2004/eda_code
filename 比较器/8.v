`timescale 1ns / 1ps
// 四位比较器
module com_4(
    input [3:0]  idata_a,
    input [3:0]  idata_b,
    input [2:0] icom,
    output reg [2:0] odata
);
    always @* begin
        if ( idata_a >  idata_b) begin
            odata = 3'b001;
        end else if ( idata_a <  idata_b) begin
            odata = 3'b100;
        end else begin
            case (icom)
                3'b100: odata = 3'b100;
                3'b001: odata = 3'b001;
                3'b010: odata = 3'b010;
                default: odata = 3'b000;
            endcase
        end
    end
endmodule

// 八位比较器
module com_8(
    input [7:0]  idata_a,
    input [7:0]  idata_b,
    output [2:0] od
);
    wire [2:0] mid;
    wire [2:0] low = 3'b010;

    com_4 comp_low4(
        . idata_a( idata_a[3:0]),
        . idata_b( idata_b[3:0]),
        .icom(low),
        .odata(mid)
    );

    com_4 comp_high4(
        . idata_a( idata_a[7:4]),
        . idata_b( idata_b[7:4]),
        .icom(mid),
        .odata(od)
    );
endmodule
