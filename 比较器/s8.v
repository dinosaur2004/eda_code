`timescale 1ns / 1ps

module tb_comparators;

    // Inputs for com_4
    reg [3:0] idata_a4;
    reg [3:0] idata_b4;
    reg [2:0] icom4;
    // Output for com_4
    wire [2:0] odata4;
    
    // Inputs for com_8
    reg [7:0] idata_a8;
    reg [7:0] idata_b8;
    // Output for com_8
    wire [2:0] od8;
    
    // Instantiate the com_4 module
    com_4 uut_com_4 (
        .idata_a(idata_a4),
        .idata_b(idata_b4),
        .icom(icom4),
        .odata(odata4)
    );
    
    // Instantiate the com_8 module
    com_8 uut_com_8 (
        .idata_a(idata_a8),
        .idata_b(idata_b8),
        .od(od8)
    );
    
    initial begin
        // Initialize inputs
        idata_a4 = 4'b0000;
        idata_b4 = 4'b0000;
        icom4 = 3'b000;
        
        idata_a8 = 8'b00000000;
        idata_b8 = 8'b00000000;
        
        // Apply test cases for com_4
        #10 idata_a4 = 4'b1010; idata_b4 = 4'b0101; icom4 = 3'b000; // idata_a > idata_b, expect odata4 = 3'b001
        #10 idata_a4 = 4'b0101; idata_b4 = 4'b1010; icom4 = 3'b000; // idata_a < idata_b, expect odata4 = 3'b100
        #10 idata_a4 = 4'b1010; idata_b4 = 4'b1010; icom4 = 3'b100; // idata_a == idata_b, icom == 3'b100, expect odata4 = 3'b100
        #10 idata_a4 = 4'b1010; idata_b4 = 4'b1010; icom4 = 3'b001; // idata_a == idata_b, icom == 3'b001, expect odata4 = 3'b001
        #10 idata_a4 = 4'b1010; idata_b4 = 4'b1010; icom4 = 3'b010; // idata_a == idata_b, icom == 3'b010, expect odata4 = 3'b010
        
        // Apply test cases for com_8
        #10 idata_a8 = 8'b10101010; idata_b8 = 8'b01010101; // idata_a[7:4] > idata_b[7:4], expect od8 = 3'b001
        #10 idata_a8 = 8'b01010101; idata_b8 = 8'b10101010; // idata_a[7:4] < idata_b[7:4], expect od8 = 3'b100
        #10 idata_a8 = 8'b10101010; idata_b8 = 8'b10101010; // idata_a == idata_b, expect od8 = 3'b010 (default value of low)

        // End simulation
        #10 $finish;
    end
    
    initial begin
        // Monitor changes to signals
        $monitor("Time=%0d idata_a4=%b idata_b4=%b icom4=%b odata4=%b | idata_a8=%b idata_b8=%b od8=%b", 
                  $time, idata_a4, idata_b4, icom4, odata4, idata_a8, idata_b8, od8);
    end

endmodule
