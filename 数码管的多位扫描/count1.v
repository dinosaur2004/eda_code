module count( 
    input clk , // 时钟信号 
    input rst_n, // 复位信号 
    input path_high, // 频率切换（高位） 
    input path_low, // 频率切换（低位） 
    
    //user interface 
    output reg [26:0] data , // 8 个数码管要显示的数值 
    output reg [ 7:0] point, // 小数点的位置
    output reg en , // 数码管使能信号 
    output reg sign // 符号位，高电平时显示负号，低电平不显示负号 
); 
//parameter define 
reg [31:0] MAX_NUM; // 计数器计数的最大值 
reg [1:0] path; 
always @* begin 
 path <= {path_high,path_low}; 
 case(path) 
    2'd0: MAX_NUM <= 32'd100; 
    2'd1: MAX_NUM <= 32'd1000; 
    2'd2: MAX_NUM <= 32'd10000; 
    2'd3: MAX_NUM <= 32'd100000; 
    default: MAX_NUM <= 32'd1000000; 
 endcase 
end 
//reg define 
reg [31:0] cnt ; // 计数器，用于计时 100ms 
reg flag; // 标志信号 
//计数器对系统时钟计数达 10ms 时，输出一个时钟周期的脉冲信号 
always @ (posedge clk or negedge rst_n) begin 
    if (!rst_n) begin 
    cnt <= 32'b0; 
    flag<= 1'b0; 
    end 
    else if (cnt < MAX_NUM - 1'b1) begin 
    cnt <= cnt + 1'b1; 
    flag<= 1'b0; 
    end 
    else begin 
    cnt <= 32'b0; 
    flag <= 1'b1; 
 end 
end 
//数码管需要显示的数据，从 0 累加到 999999 
always @ (posedge clk or negedge rst_n) begin 
    if (!rst_n)begin 
    data <= 27'b0; 
    point <=8'b00000000; 
    en <= 1'b0; 
    sign <= 1'b0; 
    end 
    else begin 
    point <= 8'b00000000; //不显示小数点 
    en <= 1'b1; //打开数码管使能信号 
    sign <= 1'b0; //不显示负号 
    if (flag) begin //显示数值每隔 0.01s 累加一次 
    if(data < 27'd99999999) 
    data <= data +1'b1; 
    else 
    data <= 27'b0; 
    end 
 end 
end 
endmodule 