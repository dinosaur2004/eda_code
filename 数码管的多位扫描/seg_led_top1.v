`timescale 1ns / 1ps

module seg_led_top(
    //global clock
    input            sys_clk  ,       // 全局时钟信号
    input            sys_rst_n,       // 复位信号（低有效）
    input path_high, // 频率切换（高位） 
    input path_low, // 频率切换（低位）

    //seg_led interface
    output    [7:0]  seg_sel  ,       // 数码管位选信号
    output    [7:0]  seg_led          // 数码管段选信号
);

//wire define
wire    [26:0]  data;                 // 数码管显示的数值
wire    [ 7:0]  point;                // 数码管小数点的位置
wire            en;                   // 数码管显示使能信号
wire            sign;                 // 数码管显示数据的符号位
wire  neg_rst_n;

//*****************************************************
//**                    main code
//*****************************************************

//计数器模块，产生数码管需要显示的数据
count u_count(
    .clk           (sys_clk  ),       // 时钟信号
    .path_high  (path_high),
    .path_low   (path_low),
//    .rst_n         (sys_rst_n),       // 复位信号
    .rst_n         (neg_rst_n),       // 复位信号
    
    .data          (data     ),       // 数码管要显示的数值
    .point         (point    ),       // 小数点具体显示的位置,高电平有效
    .en            (en       ),       // 数码管使能信号
    .sign          (sign     )        // 符号位
);

//数码管动态显示模块
seg_led u_seg_led(
    .clk           (sys_clk  ),       // 时钟信号
//    .rst_n         (sys_rst_n),       // 复位信号
    
      .rst_n         (neg_rst_n),       // 复位信号
  

    .data          (data     ),       // 显示的数值
    .point         (point    ),       // 小数点具体显示的位置,高电平有效
    .en            (en       ),       // 数码管使能信号
    .sign          (sign     ),       // 符号位，高电平显示负号(-)
    
    .seg_sel       (seg_sel  ),       // 位选
    .seg_led       (seg_led  )        // 段选
);

assign  neg_rst_n = ~sys_rst_n;


endmodule