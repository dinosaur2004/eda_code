`timescale 1ns / 1ps

module test_adder8;

    reg [7:0] ida = 8'b11111111; // 设置固定的输入值
    reg [7:0] idb = 8'b01010110; // 设置固定的输入值
    reg ic = 1'b0; // 设置固定的输入值
    wire [7:0] od;
    wire oc;

    adder8 dut(
        .ida(ida),
        .idb(idb),
        .ic(ic),
        .od(od),
        .oc(oc)
    );

    initial begin
        $monitor("Time=%t ida=%b idb=%b ic=%b od=%b oc=%b", $time, ida, idb, ic, od, oc);
        #10; // Wait for 10 time units
        $finish; // End the simulation after 10 time units
    end

endmodule
