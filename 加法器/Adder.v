`timescale 1ns / 1ps

module adder1(
    input ia,
    input ib,
    input ic,
    output os,
    output oc
);
    assign os = ia ^ ib ^ ic;
    assign oc = (ia & ib) | ((ia ^ ib) & ic); // 进位输出逻辑
endmodule

module adder8(
    input [7:0] ida,
    input [7:0] idb,
    input ic,
    output [7:0] od,
    output oc
);
    wire [6:0] idc; // 中间进位信号
    
    adder1 d1(.ia(ida[0]), .ib(idb[0]), .ic(ic), .os(od[0]), .oc(idc[0]));
    adder1 d2(.ia(ida[1]), .ib(idb[1]), .ic(idc[0]), .os(od[1]), .oc(idc[1]));
    adder1 d3(.ia(ida[2]), .ib(idb[2]), .ic(idc[1]), .os(od[2]), .oc(idc[2]));
    adder1 d4(.ia(ida[3]), .ib(idb[3]), .ic(idc[2]), .os(od[3]), .oc(idc[3]));
    adder1 d5(.ia(ida[4]), .ib(idb[4]), .ic(idc[3]), .os(od[4]), .oc(idc[4]));
    adder1 d6(.ia(ida[5]), .ib(idb[5]), .ic(idc[4]), .os(od[5]), .oc(idc[5]));
    adder1 d7(.ia(ida[6]), .ib(idb[6]), .ic(idc[5]), .os(od[6]), .oc(idc[6]));
    adder1 d8(.ia(ida[7]), .ib(idb[7]), .ic(idc[6]), .os(od[7]), .oc(oc));
endmodule



